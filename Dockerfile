# Dockerfile References: https://docs.docker.com/engine/reference/builder/
############################
# STEP 1 build executable binary
############################
FROM golang:1.13.12-alpine AS builder

RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*

# Create appuser.
RUN adduser -D -g '' appuser

# Copy project to working directory
RUN mkdir /app
COPY . /app/
WORKDIR /app/

RUN apk update && apk add git

# Build the binary.
RUN GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o main main.go


############################
# STEP 2 build a small image
############################
FROM scratch

# import certs
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
# Import the user and group files from the builder.
COPY --from=builder /etc/passwd /etc/passwd
# Copy our static executable.
COPY --from=builder /app/main /app/main

WORKDIR /app

# Use an unprivileged user.
USER appuser

# Run the binary.
ENTRYPOINT ["/app/main"]
