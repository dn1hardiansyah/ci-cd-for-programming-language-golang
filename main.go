package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

var data []int

func add(w http.ResponseWriter, r *http.Request) {

	if len(data) == 0 {
		data = append(data, 1)
	} else {
		data = append(data, data[len(data)-1]+1)
	}
	res, _ := json.Marshal(data)
	w.Write(res)
}

func watch1(w http.ResponseWriter, r *http.Request) {
	res, _ := json.Marshal(data)
	w.Write(res)
}

func watch2(w http.ResponseWriter, r *http.Request) {
	res, _ := json.Marshal(data)
	w.Write(res)
}

func watch3(w http.ResponseWriter, r *http.Request) {
	res, _ := json.Marshal(data)
	w.Write(res)
}

func report1(w http.ResponseWriter, r *http.Request) {
	var t interface{}
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		fmt.Println("Error", err)
		return
	}
	res, _ := json.Marshal(t)
	fmt.Println("Value", string(res))
}

func report2(w http.ResponseWriter, r *http.Request) {
	var t interface{}
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		fmt.Println("Error", err)
		return
	}
	res, _ := json.Marshal(t)
	fmt.Println("Value", string(res))
}

func report3(w http.ResponseWriter, r *http.Request) {
	var t interface{}
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		fmt.Println("Error", err)
		return
	}
	res, _ := json.Marshal(t)
	fmt.Println("Value", string(res))
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/add", add)
	r.HandleFunc("/watch1", watch1)
	r.HandleFunc("/watch2", watch2)
	r.HandleFunc("/watch3", watch3)
	r.HandleFunc("/report1", report1)
	r.HandleFunc("/report2", report2)
	r.HandleFunc("/report3", report3)
	// Bind to a port and pass our router in
	http.ListenAndServe(":8090", r)
}
